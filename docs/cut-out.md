# Cut-out character procedure

## Overview
Cutout characters are a relatively quick way to animate characters, as
opposed to full 3D modeling, texturing, rigging, etc. Many productions
use them exclusively, and advanced tools exist for such purposes, such
as DUIK Bassel and Toon Boom.

These tools are meant to animate in a 2D environment. By contrast,
ours allows one to animate the character inside Blender’s 3D
environment, combining it with 3D sets, camera motions and 3D
characters in a single shot.

However, although our tools make character creation quite fast, it is
limited in scope: you won’t easily create a deformable body or
rotating face, since it was always designed for secondary or tertiary
characters.

This cut-out autorig, though simpler than fully 3D characters, is
still a complex system involving several interdependant add-ons in
different programs, from 2D drawing to 3D animation.

The process basically takes place in 6 steps:

1. draw the character, on paper or digitally
2. cut each animatable piece in a 2D drawing application
3. import each layer into Blender
4. create a basic armature, and place it according to the character’s
   form
5. from this basic armature, auto-generate an advanced one, with
   controllers to simplify animation
6. parent each layer to its corresponding bone in the armature.

At this point, the rigged characted is ready to animate. Here are a
few features of this auto-rig system:

- IK/FK switch for limbs, and snapping between them;
- easy creation of props, with simple FK, IK, or fake dynamic parts;
- member and plane reordering interface.

## Installation
The following must be downloaded and installed to the `addons`
directory (see
[documentation](https://docs.blender.org/manual/en/latest/advanced/blender_directory_layout.html)):

- [2D animation tools](https://github.com/jasperges/2d_animation_tools)
- [parent plane to bone](https://github.com/LesFeesSpeciales/pantin-tools/blob/master/parent_planes_to_bones.py)

Additionally, this is a Rigify “add-on add-on”, the cutout feature set
which must be installed to the `rigify` directory next to the `addons`
one.

## Character drawing and cutting out
Cut-out characters are drawn and cut inside a 2D drawing program such
as Krita or Photoshop.

The basic principle of cut-out characters, as is implied by their
name, is that a drawing is cut into pieces, which are then articulated
together around joints. The characters can be either drawn in full and
then cut, or directly drawn onto different pieces.

In our system, we use layers to simulate these different pieces. To
create a joint, you’d then draw the two members, each with a
half-circle overlapping the other.

### Drawing joints

Here is an explanation of the basic principle of the drawing of
joints, so that they will be correctly articulated later on.

![Left part of the joint](L.png)

The left part of the joint.

![Right part of the joint](R.png)

The right part of the joint.

![Left part over right part of the joint](LR.png)

Both parts of the joint. As we can see, they are exactly overlapping
over a circle. When the joint rotates, both parts will remain tangent
to the circle, ensuring continuity.

![Joint, overlayed with the center of rotation](joint.png)

The cross goes through the centre of the articulation. The joint of
the rig will be located precisely at this centre.

![One part of the articulation rotated around the centre](joint_rotated.png)

One part of the articulation rotated around the centre.

![The whole articulation filled with black. The joint is invisible,
since it rotates around a circle to which both parts of the joint are tangent.](joint_rotated_no_line.png)

The whole articulation filled with black. The joint is invisible,
since it rotates around a circle to which both parts of the joint are
tangent.

### Template
In order to simplify this step as well as the following ones, we
include a template containing many empty layers and groups, sorted in
a hierarchy mirroring the rig in Blender. When starting a new
character, it is advised to start from this file and resize it to have
enough resolution. The joint layers can be used as a visual help:
duplicate them and place at the actual joint. The drawing will then
generally be cut around the circle, as shown in the pictures above.

Each piece can be cut, and merged with the correctly-named layer, so
that the hierarchy is kept.

At any time, the rig can be tested by selecting one of the group
layers called `_Grp`, and rotating it, making sure that the pivot is
at the center of the joint circle. For instance, if you rotate the
`Arm`, the whole limb will follow, including the forearm and hand.

When the layers are connected to the armature’s bones in Blender, the
names can be automatically matched.

## Rig creation
### Blender import
Once the character has been drawn in the 2D application, it should be
saved as a PSD (Photoshop) document, and imported into Blender using
the PSD importer. This will create as many objects as there are layers
in the PSD.

In order to be imported at the right resolution, a proper scale should
be selected.

### Subrigs
The rig process uses the Rigify add-on, included with Blender. This
add-on works in two phases: creating a basic armature (called
“metarig”), which simply defines the rig structure and joint
locations. When the basic armature is created, it will be used to
automatically generate the final rig, a complex armature comprising
many systems to help animate the character.

A “metarig” is composed of multiple subrigs (called simply “rigs” in
Rigify), corresponding roughly to each limb or body part. Each of
these subrigs have settings. You can access them by selecting the bone
in Pose Mode, and going to the Bone Properties, under the Rigify Type
Panel.

Each Rig Type expects a chain of multiple bones. For instance, an
`Arm` Type may be composed of a three-bone chain: the arm, forearm,
and hand. The Type is only assigned to the most upper bone, so in this
example, the arm bone is the one receiving the subrig settings.

![Example rig type for the arm](rigify_type.png)

Example rig type for the arm

More info may be found in [the official Rigify
documentation](https://docs.blender.org/manual/en/latest/addons/rigging/rigify/).

### Basic armature, or _metarig_
To create this basic, “metarig”, either add and place each bone in
Edit Mode, then set the Rigify properties in Pose Mode, or use a
preset metarig:

![Add a metarig by calling SHIFT + A in the 3D View.](add_metarig.png)

Add a metarig by calling SHIFT + A in the 3D View.

- In the 3D View, Add Armature → Pantin → Pantin;
  - This creates a basic rig.
- Go into front, orthographic view by pressing `1` and `5` on the
  numeric pad;
- Place each joint according to the drawing of the character, at the
  intersection of the two parts of the limb;

## Rig generation
Once the basic armature matches the drawing, the final rig can be
generated.

The generation is handled by the Rigify add-on. To generate the rig,
select the metarig, then go to the Obejct Properties, Rigify Buttons
panel, and hit Generate Rig.

![Generate Rig](generate_rig.png)

Button to generate rig.

## Plane parenting
Once the planes have been imported into Blender’s 2D space, and once
the rig is correctly positionned and built, those two things still
need to be connected. To connect geometry and rigs, riggers usually
use weight painting, so that each bone can more or lesse influence
each vertex. In our case, there is no deformation, and the multiple
moving parts are separate objects. We can thus get away with simple
parenting. To do that, we could switch to the rig’s rest pose, and
then manually parent each plane to its respective DEF-ormation bone.

Since this is quite simple but extremely tedious when many characters
are being made, an operator can be called to do it effectively for a
whole character. This operator, `Parent planes to bones`, lives in its
own add-on, and is accessible in the 3D View’s tool bar when the rig
is selected.

## Member order
Another fundamental aspect of cut-out character is that, historically,
they were monochrome, likely black on white, like a shadow puppet.
Now, they are more often that not coloured. Which piece is on top thus
takes a great importance.

For some technical reasons, ordering these planes should not be done
manually by moving the meshes in the 3D View. Instead, a special
interface exposing them was designed. It allows reordering the planes
easily, like layers in a 2D program. They are grouped by “members”,
which are the Rigify subrigs from before. Each member can be moved
back or front using its arrows, and inside it, each plane can
similarly be moved back or front.

![Member order user interface](members_order.png)

Member order user interface


## Importing characters
Characters, like other assets, are usually created in their own
library files, and then imported to or referenced from the shot files.
In Blender, both solutions can be used, called respectively _append_
and _link_.

One thing to be careful about is that in order to work properly, the
armatures generated by Rigify absolutely need a script generated at
the same time as the final rig. This script is usually called
`rig_ui.py`. It must be linked or appended from the library file, and
auto-loaded (_register_ed) by going to the text editor, selecting this
script, and going to the menu Text → Register.
